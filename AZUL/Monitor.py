from rewardBasedPlayer import get_max_difference


class Monitor:
    LIST = []

    def __init__(self, move, values, act_id, round_id, brunches, move_len, times, nodes):
        self.move = move
        self.values = values
        self.act_id = act_id
        self.dv = get_max_difference(values, act_id)
        self.round_id = round_id
        self.decrease = False
        self.move_len = move_len
        self.times = times
        self.nodes = nodes
        #self.search_time = search_time
        Monitor.LIST.append(self)

        if len(Monitor.LIST) >= 2:
            last_monitor = Monitor.LIST[-2]
            if self.round_id == last_monitor.round_id:
                self.action_id = last_monitor.action_id + 1
            else:
                self.action_id = 0
            self.decrease = self.is_decrease(last_monitor)
        else:
            self.action_id = 0

        self.brunches = brunches

    def is_decrease(self, lm):
        if self.dv < lm.dv-0.1:
            return True
        return False

    def __str__(self):
        # string = 'v:{:4} {:4} dv:{:4} r:{} a:{} b:{:3} {:3} d:{:5}'.format(
        # string='r:{} a:{} b:{:3} {:3} l:{:2}  t:{:4} n:{:4} d:{:5}'.format(
        # string = 'b:{:3} {:3} l:{:2}  t:{:4} n:{:4} d:{:1}'.format(
        string = ' r:{} a:{} dv:{:5} b:{:3} {:3} l:{:2}  t:{:4} n:{:4} d:{:1}'.format(
            self.round_id,
            self.action_id,
            round(self.dv, 1),
            self.brunches[0],
            self.brunches[1],
            self.move_len,
            self.times,
            self.nodes,
            self.decrease
        )
        return string


def MonitorsToString():
    string = ''
    for m in Monitor.LIST:
        # string += ('\n' if m.action_id == 0 else '') + str(m)+'\t'
        if m.decrease is True:
            print("\033[0;31;47m{}\033[0m".format(str(m)))
        else:
            print("\033[0;30;47m{}\033[0m".format(str(m)))

